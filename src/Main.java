import javax.swing.*;
import java.awt.*;


public class Main {

    public static void main (String [] args){


        JFrame startFrame = new JFrame("Cloud Rider");

        int centerX = (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 7);
        int centerY = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() / 15);

        Panel panel = new Panel();


        startFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// закрытие окна при клике крестика
        startFrame.setContentPane(panel); // перенос в фрейм панели с GamePanel
        //startFrame.add(panel);
        startFrame.setLocationRelativeTo(null);//положение фрейма по центру
        startFrame.pack();//размер фрейма как и размер его компонентов
        startFrame.setLocation(centerX,centerY);

        panel.start();// запускаем поток панели
// окно видимо
        startFrame.add(new Panel());
        startFrame.setVisible(true);


        startFrame.setSize(1280,920);


// окно видимо
    }
}