import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;


public class Panel extends JPanel implements Runnable {




    public static enum STATES{PLAY, MENUE};
    public static STATES state = STATES.MENUE;

    JTextField filed = new JTextField();

    public static int WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width;
    public static int HEIGHT = Toolkit.getDefaultToolkit().getScreenSize().height;


    public static int mouseX;
    public static int mouseY;

    public static boolean chik = true; // цикл While

    public static boolean board = false; // цикл While


    public boolean FlyRun = false;

    private int FPS;//
    private double millisToFPS;// fps в миллсек
    private int timer = FPS;// таймер fps
    private int sleepTime; //сколько он будет спать
    private static Thread thread; // Создаем поток- ссылка на обьект класса Thread


    private BufferedImage image;
    private Graphics2D g;

    GameBack gameBack = new GameBack();
    Listeners listeners = new Listeners();
    Asher asher= new Asher();
    Obstacles obstacles= new Obstacles();
    public static ArrayList<Image> cloudsI;
    Menue menue = new Menue();
    Gameboard gameboard= new Gameboard();
    FrontPicture frontPicture=new FrontPicture();




    public Panel(){

        super();
        setFocusable(true);
        requestFocus();


        addKeyListener(new Listeners());
        addMouseListener(new Listeners());// добавляем обработчик событий клик мышь
        addMouseMotionListener(new Listeners());


    }


    public  void start() {
        thread = new Thread((Runnable) this);
        thread.start();// запускаем поток
    }

    @Override
    public void run(){
        FPS = 30; // задаем желаемый FPS
        millisToFPS = 1000 / FPS; //пересчет в миллисек
        sleepTime = 0;

        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        cloudsI = new ArrayList<Image>();
        cloudsI.add(obstacles.b1);
        cloudsI.add(obstacles.b3);
        cloudsI.add(obstacles.b4);

        g = (Graphics2D) image.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);



        Audio audio = new Audio("core/assets/music.wav", 0);
        audio.play();// играть
        audio.setVolume();// громкость воспроизведения
        audio.repeat();// повторять
        audio.sound();


        while (true) { // игровой цикл

            if(state.equals(STATES.MENUE)){

                menue.menueDraw(g);
                menue.play.drawBut(g);
                menue.update(menue.play);

                frontPicture.update();
                frontPicture.AnimDraw(g);
                gameDraw();


            }


            if (state.equals(STATES.PLAY)) {



                gameBack.GameDraw(g);
                gameBack.update();
                obstacles.update();



                if (asher.Fly1 == true || asher.Fly2 == true) {

                    asher.AnimDraw(g);
                    asher.flyUpdate();
                }else {
                    asher.AnimDraw(g);
                    asher.update();
                }

                render();
                update();
                gameDraw();



            }

            double timerFPS = System.nanoTime();// присвоим текущ время


            timerFPS = (System.nanoTime()-timerFPS)/1000000;//сколько прошло миллсек на операции выше
            if(millisToFPS > timerFPS){
                sleepTime = (int)(millisToFPS - timerFPS);
            } else sleepTime = 1;

            try {
                thread.sleep(sleepTime); //засыпаем на ... мс
            } catch (InterruptedException ex) { //если не удается заснуть- исключение
                ex.printStackTrace();
            }
            timerFPS = 0;// обнуляем таймер
            sleepTime = 1;// обновляем время сна

        }
    }

    public void render(){
        obstacles.Y1Draw(g);
        obstacles.drawClouds1(g);
        obstacles.drawClouds3(g);
        obstacles.drawClouds4(g);

        if(gameBack.GameOver == true){

            gameboard.boardDraw(g);
            gameboard.scoreDraw(g);
            menue.cross.drawBut(g);
            menue.CrossBoard(menue.cross);




        }
    }




    public void update() {
        Iterator<Image> i = cloudsI.iterator();

        while(i.hasNext()){
            Image e = i.next();
            if(asher.getRect().intersects(obstacles.getRect1())){

                gameBack.GameOver = true;

            }
            if(asher.getRect().intersects(obstacles.getRect2())){
                gameBack.GameOver = true;

            }
            if(asher.getRect().intersects(obstacles.getRect3())){
                gameBack.GameOver = true;

            }
        }
        obstacles.BoxUpdate();

        if(gameBack.GameOver == true){

            gameboard.overUpdate();


        }


    }



    public void gameDraw(){

        Graphics g2 = this.getGraphics();// переопред Graphics2d на Graphics
        g2.drawImage(image, 0, 0, null);// рисуем
        g2.dispose();// команда для уборщика мусора
    }
}