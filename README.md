In this game you have to dodge obstacles in the form of thunderclouds.

To control the character, use the keys: W-for lifting up, S-for planning down.

To interact with interface objects, use the left mouse button.